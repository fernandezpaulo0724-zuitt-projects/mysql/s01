==Mini Activity Link==

https://docs.google.com/document/d/1iuGxlsbQdBVgVNXLQOOFthUP6tk91722NWUrUwxTkXA/edit?usp=sharing

=Activity Solutions=
1. List the books authored by Marjorie Green
- The Busy Executives Database Guide, You Can Combat Computer Stress!
2. List the books authored by Michael O'Leary
- Cooking with Computers, Untitled (TC7777)
3. Write the author/s of "The Busy Executives Database Guide"
- Marjorie Green, Abraham Bennet
4. Identify the publisher of "But is it User Friendly?"
- Cheryl Carson
5. List the books published by Algodata Infosystems
- The Busy Executives Database Guide, Cooking with Computers, Straight Talk About Computers, But Is It User Friendly?, Secrets of Silicon Valley, Net Etiquette